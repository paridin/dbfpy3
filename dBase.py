# -*- coding: utf-8 -*-
# In a future you can use this tool in python 3.x
from __future__ import print_function
# This Tool is used to migrate from dBase to ORM in DJANGO
# Create on 04/Sep/14
# Last update on
# Contact roberto.estrada.ext@sicam.mx; restrada@paridin.com
# Depends Libs ['dbfpy-2.3.0 or above',]
__author__ = 'Roberto Estrada'
__version__ = "$Revision: e98737176f1d $"
# $Source$
import os
#import simplejson as json
import ujson as json
from collections import OrderedDict
from dbfpy3 import dbf

class MigrationFromDBase:
    '''
        This class works for migrate the dBase to orm django
        actually tested only on pypy
    '''
    '''
     Configure section
     TABLES_DIR Specifies the path of the current directory
    '''
    TABLES_PATH = ('/').join(os.path.abspath(__file__).split('/')[0:-1]) + '/tables'

    def fetch_dbase_table(self, table, keys=None, path=None):
        ''' Fetches rows from a dBase Table

        Retrieves rows pertaining to the given keys from the Table instance

        :param table:  the name of the table
        :param keys: the list of keys
        :param path: Optional value, if you don't specify this value
         it will take it the default TABLES_PATH
        :return list: Return a list with internal dictionary
        '''

        if path is not None:
            if os.path.isdir(path):
                if path[-1] == '/':
                    self.TABLES_PATH = path[0:-1]
                else:
                    self.TABLES_PATH = path
            else:
                self.error(-1)
        db = dbf.Dbf("%s/%s" % (self.TABLES_PATH, table))
        return list(map(lambda record: record.as_json(), db))

    def fetch_dbase_keys(self, table):
        ''' Fetches keys from a dBase Table

        Retrieves keys pertaining to the given table from the instance

        :param table:  the name of the table
        :return list: Return a list with keys from the table
        '''
        db = dbf.Dbf("%s/%s" % (self.TABLES_PATH, table))
        dbase_keys = []
        for r in db.field_names:
            dbase_keys.append(r)
        return dbase_keys

    def set_path(self, path):
        self.TABLES_DIR = path

    def error(self,id):
        if id == -1:
            error = "Error the path is not a directory"

class UniqueJson(object):
    def __init__(self, json, key):
        all_ids = [each[key] for each in json]
        self.json = [json[all_ids.index(id)] for id in set(all_ids)]

    def __iter__(self):
        return iter(self.json)

    def __len__(self):
        return len(self.json)

# Testing tool
if __name__ == '__main__':
    import cProfile as profile
    m = MigrationFromDBase()
    profile.run('UniqueJson(m.fetch_dbase_table("STATUS.DBF"), "NOMOV")')