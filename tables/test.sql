CREATE TABLE RESPONS (
   RESPONS INTEGER, 
   QUIEN CHAR(10), 
   NOMBRE CHAR(35), 
   NUMAPOD CHAR(10), 
   PRIMER CHAR(15), 
   SEGUNDO CHAR(15), 
   PATERNO CHAR(15), 
   MATERNO CHAR(15), 
   NIVACC INTEGER, 
   AREA INTEGER, 
   ACTIVOPPM CHAR(1), 
   PUEDOFACT CHAR(1), 
   ALTAFACTPR CHAR(1));

INSERT INTO RESPONS
  (RESPONS, QUIEN, NOMBRE, NUMAPOD, PRIMER, SEGUNDO, PATERNO, MATERNO, NIVACC, AREA, ACTIVOPPM, PUEDOFACT, ALTAFACTPR)
VALUES
  (1, "MEstevaZ", "Miguel A. Esteva Zuniga", "6283", "Miguel", "A.", "Esteva", "Zuniga", 4, 0, "S", "N", "N");
INSERT INTO RESPONS
  (RESPONS, QUIEN, NOMBRE, NUMAPOD, PRIMER, SEGUNDO, PATERNO, MATERNO, NIVACC, AREA, ACTIVOPPM, PUEDOFACT, ALTAFACTPR)
VALUES
  (2, "Doris W", "Doris Wurts", "", "", "", "", "", 4, 10, "N", "N", "N");
INSERT INTO RESPONS
  (RESPONS, QUIEN, NOMBRE, NUMAPOD, PRIMER, SEGUNDO, PATERNO, MATERNO, NIVACC, AREA, ACTIVOPPM, PUEDOFACT, ALTAFACTPR)
VALUES
  (3, "Antonio T", "Antonio  Torres Arellano", "", "Antonio", "", "Torres", "Arellano", 4, 2, "S", "N", "N");
INSERT INTO RESPONS
  (RESPONS, QUIEN, NOMBRE, NUMAPOD, PRIMER, SEGUNDO, PATERNO, MATERNO, NIVACC, AREA, ACTIVOPPM, PUEDOFACT, ALTAFACTPR)
VALUES
  (4, "Miguel EW", "Miguel Esteva Wurts", "54102", "", "", "", "", 5, 2, "S", "S", "N");
INSERT INTO RESPONS
  (RESPONS, QUIEN, NOMBRE, NUMAPOD, PRIMER, SEGUNDO, PATERNO, MATERNO, NIVACC, AREA, ACTIVOPPM, PUEDOFACT, ALTAFACTPR)
VALUES
  (5, "Humberto L", "Humberto Francisco Lopez Avila", "", "Humberto", "Francisco", "Lopez", "Avila", 4, 4, "S", "N", "S");
INSERT INTO RESPONS
  (RESPONS, QUIEN, NOMBRE, NUMAPOD, PRIMER, SEGUNDO, PATERNO, MATERNO, NIVACC, AREA, ACTIVOPPM, PUEDOFACT, ALTAFACTPR)
VALUES
  (6, "Georgina E", "Georgina Esteva Wurts", "39538", "", "", "", "", 4, 1, "S", "N", "N");
INSERT INTO RESPONS
  (RESPONS, QUIEN, NOMBRE, NUMAPOD, PRIMER, SEGUNDO, PATERNO, MATERNO, NIVACC, AREA, ACTIVOPPM, PUEDOFACT, ALTAFACTPR)
VALUES
  (7, "Liliana C", "Liliana Chavez Nava Galifa Galifa", "", "Liliana", "Chavez Nava", "Galifa", "Galifa", 4, 1, "S", "N", "N");
INSERT INTO RESPONS
  (RESPONS, QUIEN, NOMBRE, NUMAPOD, PRIMER, SEGUNDO, PATERNO, MATERNO, NIVACC, AREA, ACTIVOPPM, PUEDOFACT, ALTAFACTPR)
VALUES
  (9, "Dinorah A", "Dinorah Acosta", "", "", "", "", "", 3, 2, "S", "N", "N");
INSERT INTO RESPONS
  (RESPONS, QUIEN, NOMBRE, NUMAPOD, PRIMER, SEGUNDO, PATERNO, MATERNO, NIVACC, AREA, ACTIVOPPM, PUEDOFACT, ALTAFACTPR)
VALUES
  (11, "Evelia C", "Evelia Caballero", "", "", "", "", "", 3, 2, "S", "N", "N");
INSERT INTO RESPONS
  (RESPONS, QUIEN, NOMBRE, NUMAPOD, PRIMER, SEGUNDO, PATERNO, MATERNO, NIVACC, AREA, ACTIVOPPM, PUEDOFACT, ALTAFACTPR)
VALUES
  (13, "Victor T", "Victor Torres", "", "", "", "", "", 3, 1, "S", "S", "S");
INSERT INTO RESPONS
  (RESPONS, QUIEN, NOMBRE, NUMAPOD, PRIMER, SEGUNDO, PATERNO, MATERNO, NIVACC, AREA, ACTIVOPPM, PUEDOFACT, ALTAFACTPR)
VALUES
  (14, "Contabilid", "Laura Luna", "", "Laura", "", "Luna", "", 0, 0, "S", "N", "N");
INSERT INTO RESPONS
  (RESPONS, QUIEN, NOMBRE, NUMAPOD, PRIMER, SEGUNDO, PATERNO, MATERNO, NIVACC, AREA, ACTIVOPPM, PUEDOFACT, ALTAFACTPR)
VALUES
  (15, "Moira AE", "Moira Andrea Esteva Wurts", "", "Moira", "Andrea", "Esteva", "Wurts", 4, 3, "S", "S", "S");
INSERT INTO RESPONS
  (RESPONS, QUIEN, NOMBRE, NUMAPOD, PRIMER, SEGUNDO, PATERNO, MATERNO, NIVACC, AREA, ACTIVOPPM, PUEDOFACT, ALTAFACTPR)
VALUES
  (17, "Nereo G", "Nereo Gutierrez", "", "Nereo", "", "Gutierrez", "", 4, 10, "S", "N", "S");
INSERT INTO RESPONS
  (RESPONS, QUIEN, NOMBRE, NUMAPOD, PRIMER, SEGUNDO, PATERNO, MATERNO, NIVACC, AREA, ACTIVOPPM, PUEDOFACT, ALTAFACTPR)
VALUES
  (18, "Mauricio E", "Mauricio  Escoto Martinez de Cas", "", "Mauricio", "", "Escoto", "Martinez de Cas", 4, 4, "S", "N", "N");
INSERT INTO RESPONS
  (RESPONS, QUIEN, NOMBRE, NUMAPOD, PRIMER, SEGUNDO, PATERNO, MATERNO, NIVACC, AREA, ACTIVOPPM, PUEDOFACT, ALTAFACTPR)
VALUES
  (19, "Rafael G", "Rafael Gonzalez Bustillos", "", "", "", "", "", 4, 1, "S", "N", "N");
INSERT INTO RESPONS
  (RESPONS, QUIEN, NOMBRE, NUMAPOD, PRIMER, SEGUNDO, PATERNO, MATERNO, NIVACC, AREA, ACTIVOPPM, PUEDOFACT, ALTAFACTPR)
VALUES
  (20, "no funcion", "esta no funciona para la opciones", "", "", "", "", "", 3, 2, "N", "N", "N");
INSERT INTO RESPONS
  (RESPONS, QUIEN, NOMBRE, NUMAPOD, PRIMER, SEGUNDO, PATERNO, MATERNO, NIVACC, AREA, ACTIVOPPM, PUEDOFACT, ALTAFACTPR)
VALUES
  (27, "Andres E", "Andr's Esteva Wurts", "", "", "", "", "", 5, 2, "S", "S", "N");
INSERT INTO RESPONS
  (RESPONS, QUIEN, NOMBRE, NUMAPOD, PRIMER, SEGUNDO, PATERNO, MATERNO, NIVACC, AREA, ACTIVOPPM, PUEDOFACT, ALTAFACTPR)
VALUES
  (46, "Miguel C", "Miguel  Castillo", "", "Miguel", "", "Castillo", "", 4, 1, "S", "S", "S");
INSERT INTO RESPONS
  (RESPONS, QUIEN, NOMBRE, NUMAPOD, PRIMER, SEGUNDO, PATERNO, MATERNO, NIVACC, AREA, ACTIVOPPM, PUEDOFACT, ALTAFACTPR)
VALUES
  (47, "Anabel F", "Anabel Fernandez C", "", "", "", "", "", 4, 10, "S", "S", "S");
INSERT INTO RESPONS
  (RESPONS, QUIEN, NOMBRE, NUMAPOD, PRIMER, SEGUNDO, PATERNO, MATERNO, NIVACC, AREA, ACTIVOPPM, PUEDOFACT, ALTAFACTPR)
VALUES
  (48, "Tania GH", "Tania Alejandra Gorostieta Hurtado", "", "Tania", "Alejandra", "Gorostieta", "Hurtado", 4, 3, "S", "S", "S");
INSERT INTO RESPONS
  (RESPONS, QUIEN, NOMBRE, NUMAPOD, PRIMER, SEGUNDO, PATERNO, MATERNO, NIVACC, AREA, ACTIVOPPM, PUEDOFACT, ALTAFACTPR)
VALUES
  (49, "David Z", "David Zamores", "", "", "", "", "", 3, 1, "N", "N", "S");
INSERT INTO RESPONS
  (RESPONS, QUIEN, NOMBRE, NUMAPOD, PRIMER, SEGUNDO, PATERNO, MATERNO, NIVACC, AREA, ACTIVOPPM, PUEDOFACT, ALTAFACTPR)
VALUES
  (50, "Felipe G", "Felipe Gutierrez", "", "", "", "", "", 3, 4, "S", "N", "N");
INSERT INTO RESPONS
  (RESPONS, QUIEN, NOMBRE, NUMAPOD, PRIMER, SEGUNDO, PATERNO, MATERNO, NIVACC, AREA, ACTIVOPPM, PUEDOFACT, ALTAFACTPR)
VALUES
  (57, "Blanca SG", "Blanca Sofia Sanchez Gonzalez", "", "Blanca", "Sofia", "Sanchez", "Gonzalez", 4, 6, "S", "N", "");
INSERT INTO RESPONS
  (RESPONS, QUIEN, NOMBRE, NUMAPOD, PRIMER, SEGUNDO, PATERNO, MATERNO, NIVACC, AREA, ACTIVOPPM, PUEDOFACT, ALTAFACTPR)
VALUES
  (60, "JoseM H", "Jose Manuel Hernandez", "", "Jose", "Manuel", "Hernandez", "", 4, 2, "S", "N", "N");
INSERT INTO RESPONS
  (RESPONS, QUIEN, NOMBRE, NUMAPOD, PRIMER, SEGUNDO, PATERNO, MATERNO, NIVACC, AREA, ACTIVOPPM, PUEDOFACT, ALTAFACTPR)
VALUES
  (61, "no funcion", "no funciona", "", "Pamela", "Elizabeth", "Jimenez", "Cruz", 4, 2, "N", "N", "N");
INSERT INTO RESPONS
  (RESPONS, QUIEN, NOMBRE, NUMAPOD, PRIMER, SEGUNDO, PATERNO, MATERNO, NIVACC, AREA, ACTIVOPPM, PUEDOFACT, ALTAFACTPR)
VALUES
  (62, "Alfonso HH", "Alfonso Hernandez Hinojosa", "", "Alfonos", "", "Hernandez", "Hinojosa", 4, 2, "S", "N", "N");

COMMIT;

